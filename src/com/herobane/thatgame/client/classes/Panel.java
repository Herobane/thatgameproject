package com.herobane.thatgame.client.classes;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class Panel extends JPanel {

	public Panel() {
		
	}
	
	public void paintComponent(Graphics g) {
		g.setColor(new Color(255, 255, 255));
		g.fillRect(0, 0, 680, 440);
		g.drawImage(new ImageIcon("src/com/herobane/thatgame/client/textures/background.png").getImage(), 0, 0, null);
		g.drawImage(new ImageIcon("src/com/herobane/thatgame/client/textures/ground.png").getImage(), 0, 380, null);
		g.drawImage(new ImageIcon("src/com/herobane/thatgame/client/textures/perso.png").getImage(), Frame.xPos, Frame.yPos, null);
		
	}

}
