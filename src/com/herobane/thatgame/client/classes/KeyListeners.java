package com.herobane.thatgame.client.classes;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class KeyListeners implements KeyListener {

	public static boolean right = false;
	public static boolean left = false;
	public static boolean sprint = false;
	
	@Override
	public void keyPressed(KeyEvent e) {
		if(e.getKeyCode() == 39) {
			right = true;
		}
		
		if(e.getKeyCode() == 37) {
			left = true;
		}
		
		
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		if(e.getKeyCode() == 39) {
			right = false;
		}
		
		if(e.getKeyCode() == 37) {
			left = false;
		}
				
	}

	@Override
	public void keyTyped(KeyEvent e) {
		
	}
	
}
