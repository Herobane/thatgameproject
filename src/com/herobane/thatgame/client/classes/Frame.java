package com.herobane.thatgame.client.classes;

import javax.swing.JFrame;

@SuppressWarnings("serial")
public class Frame extends JFrame {

	public static int xPos = 0;
	public static int yPos = 333;
	public static int amoPos = 0;
	
	Panel panel = new Panel();
	KeyListeners kl = new KeyListeners();
	MouseListeners ml = new MouseListeners();
	
	public Frame() {
		this.setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setTitle("ThatGame");
		this.setResizable(false);
		this.setSize(686,  468);
		this.setLocationRelativeTo(null);
		this.setContentPane(panel);
		this.addKeyListener(kl);
		this.addMouseListener(ml);
		trame();
	}
	
	public void trame() {
		while(true) {
			try {
				Thread.sleep(15);
			} catch (InterruptedException e) {
				System.out.println("Error : InterruptedException");
			}
			if(KeyListeners.right) xPos++;
			if(KeyListeners.left) xPos--;
			if(KeyListeners.sprint) xPos+=2;
			
			panel.repaint();
		}
	}

}
